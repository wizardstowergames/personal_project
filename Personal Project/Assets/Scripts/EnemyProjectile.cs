using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    private float speed = 30;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * speed, Space.World);//move left over time at speed
    }

    //check for collider entry
    private void OnCollisionEnter(Collision collision)
    {
        //if collider entered has 'Ground' tag...
        if (collision.gameObject.CompareTag("Ground"))
        {
            Destroy(gameObject);//destroy this gameobject
        }

        //if collider entered has "LandingPad" tag...
        if (collision.gameObject.CompareTag("LandingPad"))
        {
            Debug.Log("EnemyProjectile has Hit Landing Pad");//send message to debug console
            LandingPadController landingPadControllerScript = 
                collision.gameObject.GetComponent<LandingPadController>(); //attach LandingPad script to variable

            landingPadControllerScript.health--;//remove 1 health from landingpad script 'health' value 
            Destroy(gameObject);//destroy this object
        }

        //if collider entered has "LandingPad" tag...
        if (collision.gameObject.CompareTag("Tower"))
        {
            Debug.Log("EnemyProjectile has hit Tower");//send message to debug console
            TowerController towerScript = 
                collision.gameObject.GetComponent<TowerController>();//attach TowerController script to variable

            towerScript.health--;//remove 1 health from TowerController scripts health value
            Destroy(gameObject);//destroy this gameobject
        }

        //if collider entered has "Player" tag...
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("EnemyProjectile has hit Player");//send message to debug console
            PlayerController playerScript = 
                collision.gameObject.GetComponent<PlayerController>();//attach PlayerController script to variable

            playerScript.health--;//remove 1 health from PlayerController scripts health value
            Destroy(gameObject);//destroy this game object
        }

        //if collider entered has "TowerTurret" tag...
        if (collision.gameObject.CompareTag("TowerTurret"))
        {
            Debug.Log("EnemyProjectile has hit TowerTurret");//send message to debug console
            TowerTurretController towerTurretScript = 
                collision.gameObject.GetComponent<TowerTurretController>();//attach TowerTurretController script to variable

            towerTurretScript.health--;//remove 1 health from TowerTurretController scripts health value
            Destroy(gameObject);//destroy this gameobject
        }
    }
}
