using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandingPadController : MonoBehaviour
{
    public int health = 3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if 'health' is equal to 0...
        if (health == 0)
        {
            Destroy(gameObject);//destroy this gameObject
        }
    }
}
