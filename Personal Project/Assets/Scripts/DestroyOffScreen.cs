using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOffScreen : MonoBehaviour
{
    private float screenLimitX = 35;    
    private float screenLimitY = 25;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if this gameObject goes beyond the set limits of X and Y axis...
        if (transform.position.x < -screenLimitX || transform.position.x > screenLimitX ||
            transform.position.y < -screenLimitY || transform.position.y > screenLimitY)
        {
            Destroy(gameObject);//destroy this gameObject
        }
    }
}
