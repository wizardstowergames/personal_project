using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBomb : MonoBehaviour
{
    private float speed = 400;    
    private float rotationSpeed = 500;
    private float fallSpeed = 300;
    private Rigidbody bombRb;


    // Start is called before the first frame update
    void Start()
    {
        bombRb = GetComponent<Rigidbody>();//attach rigidbody component to variable
        bombRb.centerOfMass = new Vector3(-5, 0, 0);//change center of mass -5 on X axis
    }

    // Update is called once per frame
    void Update()
    {
        bombRb.AddForce(Vector3.left * Time.deltaTime * speed);//move left over time at 'speed'
        bombRb.AddForce(Vector3.down * Time.deltaTime * fallSpeed);//move down over time at 'fallSpeed'
        bombRb.AddTorque(Vector3.forward * Time.deltaTime * rotationSpeed);//rotate over time at 'rotationSpeed'
    }


    //check for collider entrys
    private void OnCollisionEnter(Collision collision)
    {
        //if collider entered has "Ground" tag...
        if (collision.gameObject.CompareTag("Ground"))
        {
            Debug.Log("Bomb has hit Ground");//send message to debug console
            Destroy(gameObject);//destroy this gameObject
        }

        //if collider entered has "LandingPad" tag...
        if (collision.gameObject.CompareTag("LandingPad"))
        {
            Debug.Log("Bomb has Hit Landing Pad");//send message to debug console
            LandingPadController landingPadControllerScript = 
                collision.gameObject.GetComponent<LandingPadController>();//attach LandingPadController script to variable

            landingPadControllerScript.health--;//remove 1 from LandingPadController scripts 'health' value
            Destroy(gameObject);//destroy this gameObject
        }

        //if collider entered has "Tower" tag...
        if (collision.gameObject.CompareTag("Tower"))
        {
            Debug.Log("Bomb has hit Tower");//send message to debug console
            TowerController towerScript = 
                collision.gameObject.GetComponent<TowerController>();//attach TowerController script to variable

            towerScript.health--;//remove 1 from TowerController scripts 'health' value
            Destroy(gameObject);//destroy this gameObject
        }

        //if collider entered has "Player" tag...
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Bomb has hit Player");//send message to debug console
            Destroy(collision.gameObject);//destroy gameObject collided with
            Destroy(gameObject);//destroy this gameObject
        }

        //if collider entered has "TowerTurret" tag...
        if (collision.gameObject.CompareTag("TowerTurret"))
        {
            Debug.Log("Bomb has hit TowerTurret");//send message to debug console
            TowerTurretController towerTurretScript = 
                collision.gameObject.GetComponent<TowerTurretController>();//attach TowerTurretController script to variable

            towerTurretScript.health--;//remove 1 from TowerTurretController scripts 'health' value
            Destroy(gameObject);//destroy this gameObject
        }
    }
}
