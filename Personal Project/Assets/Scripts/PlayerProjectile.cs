using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : MonoBehaviour
{
    private float speed = 40;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * Time.deltaTime * speed);//Move right over time at speed
    }

    //check for entering colliders
    private void OnCollisionEnter(Collision collision)
    {
        //if collider entered has "Enemy" tag...
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("PlayerProjectile has hit Enemy");//send message to debug console
            Destroy(collision.gameObject);//Destroy object collided with 
            Destroy(gameObject);//Destroy this gameObject
        }

        //if collider entered has "EnemyBomber" tag...
        if (collision.gameObject.CompareTag("EnemyBomber"))
        {
            Debug.Log("PlayerProjectile has hit EnemyBomber");//send message to debug console
            EnemyBomber bomberScript = collision.gameObject.GetComponent<EnemyBomber>();//attach enemyBomber script to Variable
            bomberScript.health--;//remove 1 from health value of EnemyBomber
            Destroy(gameObject);//Destroy this gameObject
        }


        //if collider entered has "EnemyProjectile" tag...
        if (collision.gameObject.CompareTag("EnemyProjectile"))
        {
            Debug.Log("PlayerProjectile has hit EnemyProjectile");//send message to debug console
            Destroy(collision.gameObject);//Destroy object collided with 
            Destroy(gameObject);//Destroy this gameObject
        }

        //if collider entered has "EnemyBomb" tag...
        if (collision.gameObject.CompareTag("EnemyBomb"))
        {
            Debug.Log("PlayerProjectile has hit EnemyBomb");//send message to debug console
            Destroy(collision.gameObject);//Destroy object collided with 
            Destroy(gameObject);//Destroy this gameObject
        }

        //if collider entered has "TowerTurret" tag...
        if (collision.gameObject.CompareTag("TowerTurret"))
        {
            Debug.Log("PlayerProjectile has hit TowerTurret");//send message to debug console
            Destroy(gameObject);//Destroy this gameObject
        }

        //if collider entered has "Tower" tag...
        if (collision.gameObject.CompareTag("Tower"))
        {
            Debug.Log("PlayerProjectile has hit Tower");//send message to debug console
            Destroy(gameObject);//Destroy this gameObject
        }

        //if collider entered has "LandingPad" tag...
        if (collision.gameObject.CompareTag("LandingPad"))
        {
            Debug.Log("PlayerProjectile has hit LandingPad");//send message to debug console
            Destroy(gameObject);//Destroy this gameObject
        }

        //if collider entered has "Ground" tag...
        if (collision.gameObject.CompareTag("Ground"))
        {
            Debug.Log("PlayerProjectile has hit Ground");//send message to debug console
            Destroy(gameObject);//Destroy this gameObject
        }
    }
}
