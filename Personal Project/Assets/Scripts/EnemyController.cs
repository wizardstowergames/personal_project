using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private float topLimit = 14;
    private float bottomLimit = -12;
    private float moveLimitX = 15;
    private float moveLimitMod;
    private float speed = 100;
    private float floatSpeed = 200;
    private float enemyStartingYPos;
    private float enemyFloatTopLimit;
    private float enemyFloatBottomLimit;
    private bool enemyIsFloatingUp = true;
    private Rigidbody enemyRb;
        

    // Start is called before the first frame update
    void Start()
    {
        enemyRb = GetComponent<Rigidbody>(); //get Rigidbody component from gameobject
        enemyStartingYPos = transform.position.y;//assign starting location on Y axis to a variable
        enemyFloatTopLimit = enemyStartingYPos + 0.5f;//set float distance up to starting Y plus .5
        enemyFloatBottomLimit = enemyStartingYPos - 0.5f;//set float distance down to starting Y minus .5
        moveLimitMod = Random.Range(0.0f, 3.0f);//assign random number from 0-3 to offset stopping point on X axis
    }

    // Update is called once per frame
    void Update()
    {
        //if current location on X axis is greater than X limit plus random offset...
        if (transform.position.x > moveLimitX + moveLimitMod)
        {
            enemyRb.AddForce(Vector3.left * Time.deltaTime * speed);//move left across the screen
        }

        //if current position on Y axis is greater or equal to 'enemyFloatTopLimit'...
        if (transform.position.y >= enemyFloatTopLimit)
        {
            enemyIsFloatingUp = false;//set 'enemyIsFloatingUp' to false
        }

        //if current position on Y axis is less than or equal to 'EnemyFloatBottomLimit'...
        if (transform.position.y <= enemyFloatBottomLimit)
        {
            enemyIsFloatingUp = true;//set 'enemyIsFloatingUp' to true
        }

        //if current position on Y axis is less than 'FloatTopLimit' and 'enemyIsFloatingUp' is true...
        if (transform.position.y < enemyFloatTopLimit && enemyIsFloatingUp)
        {
            enemyRb.AddForce(Vector3.up * Time.deltaTime * floatSpeed);//move up over time at float speed
        }

        //if current position on Y axis is greater than 'enemyFloatBottomLimit' AND 'enemyIsFloatingUp' is false...
        if (transform.position.y > enemyFloatBottomLimit && !enemyIsFloatingUp)
        {
            enemyRb.AddForce(Vector3.down * Time.deltaTime * floatSpeed);//move down over time at float speed
        }

      

        
    }
}
