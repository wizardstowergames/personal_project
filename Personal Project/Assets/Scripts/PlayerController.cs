using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    
    
    private bool canHeal = false;
    private float horizontalInput;
    private float verticalInput;
    private float speed = 2000;
    private float leftLimit = -16.0f;
    private float rightLimit = 0.0f;
    private float topLimit = 14;
    private float bottomLimit = -12;
    private float healthCoolDown = 1f;
    private Vector3 mousePos;
    private Rigidbody playerRb;
    
    public Camera cam;
    public bool poweredUp = false;
    public int health = 5;


    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        PlayerMovement();//run player movement related code
        RestrictMovement(); //Keeps player within certain boundries on screen
        PlayerHealth();//run player health relate code


    }

    //Player Health Management
    void PlayerHealth()
    {
        //Checks if 'canHeal' is true
        if (canHeal)
        {
            healthCoolDown -= Time.deltaTime;//removes time from healing cooldown value
            if (healthCoolDown <= 0)//if cooldown value is 0...
            {
                health++;//add 1 health to player
                healthCoolDown = 1;//reset healing cooldown value
            }
        }

        //checks if player health is over 5...
        if (health >= 5)
        {
            health = 5;//reset health to 5
        }

        //if player health is 0...
        if (health <= 0)
        {
            Destroy(gameObject);//destroy player object
        }
    }

    //Player Movement Manager
    void PlayerMovement()
    {
        //Get the axis inputs from the player
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        //get the position of the mouse in the game world
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);

        //Apply force to the palyer based on inputs
        playerRb.AddForce(Vector3.up * Time.deltaTime * speed * verticalInput);
        playerRb.AddForce(Vector3.right * Time.deltaTime * speed * horizontalInput);

        //keep the player facing the mouse location
        Vector2 lookDir = mousePos - playerRb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
        playerRb.rotation = Quaternion.AngleAxis(angle, Vector3.forward);


    }

    //Keeps the Player from moving outside of set bounds
    void RestrictMovement()
    {
        if (transform.position.x <= leftLimit)
        {
            transform.position = new Vector3(leftLimit, transform.position.y, transform.position.z);
        }
        if (transform.position.x >= rightLimit)
        {
            transform.position = new Vector3(rightLimit, transform.position.y, transform.position.z);
        }
        if (transform.position.y >= topLimit)
        {
            transform.position = new Vector3(transform.position.x, topLimit, transform.position.z);
        }
        if (transform.position.y <= bottomLimit)
        {
            transform.position = new Vector3(transform.position.x, bottomLimit, transform.position.z);
        }
    }

    //checks for player entering collision with Trigger objects
    private void OnTriggerEnter(Collider other)
    {
        //If other object trigger has 'PowerUp' tag...
        if (other.gameObject.CompareTag("PowerUp"))
        {
            poweredUp = true;//set 'poweredUp' to true
            StartCoroutine(PowerUpTimer());//start coroutine for PowerUp timer
            Destroy(other.gameObject);//destroy object with trigger 'PowerUp'
        }

        //If other object trigger has 'LandingPad' tag...
        if (other.gameObject.CompareTag("LandingPad"))
        {
            canHeal = true;//set 'canHeal' to true 
        }
              
    }

    //checks for player leaving collision with Trigger objects
    private void OnTriggerExit(Collider other)
    {
        //if other object has 'LandingPad' tag...
        if (other.gameObject.CompareTag("LandingPad"))
        {
            canHeal = false; //set 'canHeal' to false
        }
    }

    //Coroutine to keep track of PowerUp timer
    IEnumerator PowerUpTimer()
    {
        yield return new WaitForSeconds(5);//wait 5 seconds, then...
        poweredUp = false;//set 'poweredUp' to false
    }
 
}
