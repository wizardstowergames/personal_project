using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerTurretController : MonoBehaviour
{
    public Camera cam;
    public int health = 2;

    private Rigidbody turretRb;
    private Vector3 mousePos;
    
    

    // Start is called before the first frame update
    void Start()
    {
        turretRb = GetComponent<Rigidbody>();//get RigidBody component of this gameObject
    }

    // Update is called once per frame
    void Update()
    {
        GameObject player = GameObject.Find("Player");//assign reference to "Player" object in scene
        PlayerController playerScript = player.GetComponent<PlayerController>();//assign PlayerController script to variable

        //if PlayerScript's poweredUp variable is true...
        if (playerScript.poweredUp == true)
        {
            mousePos = cam.ScreenToWorldPoint(Input.mousePosition);//set 'mousePos' to the current location of the mouse in the scene space

            Vector2 lookDir = mousePos - turretRb.position;// assign a vector 2 in the direction of the mouse from the location of the turret
            float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;//create a variable that is the angle from X axis converted from Rad to Deg
            turretRb.rotation = Quaternion.AngleAxis(angle, Vector3.forward);//set turret rotation to angle calculated above on Zrotation
        }

        //if PlayerScript's poweredUp variable is false...
        if (playerScript.poweredUp == false)
        {
            turretRb.rotation = Quaternion.AngleAxis(-65, Vector3.forward);//set transform rotation to -65 degrees on Zrotation
        }

        //if this objects health is equal to 0...
        if (health == 0)
        {
            Destroy(gameObject);//destroy this gameObject
        }
    }
}
